# tonnetz

A command-line tool for generating chord progressions across the 2D Tonnetz harmonic grid.<br>
See Frans Absil's excellent resources [here](https://www.fransabsil.nl/htm/tonnetz_riemannian_transformations.htm) for an introduction to the subject.

## Installation
Clone the repo and set up a virtual environment.<br>
`pip install -r requirements.txt`<br>
The only current third-party dependency is the **mido** midi library.

## Usage
> `usage: main.py [--low LOW] [--high HIGH] [--repeat REPEAT] [--midi-path MIDI_PATH] starting_chord transformation`

See the help with `python main.py --help` for more details on the syntax of the transformation string and how to format note names for the low and high constraint parameters.

## Examples
Starting at C major, do a single parallel transform to C minor.<br>
`python main.py C4M p4`

Starting at Eb minor do a series of transformations parallel -> relative -> parallel to end up at C major.<br>
`python main.py Eb4m p4r4p4`

Starting at C major, do a compound transformation to go to G minor.<br>
`python main.py C4M prl4`

Starting at C major, repeat the given set of transformations eight times, and constrain the results to a single octave range between C4 and C5.<br>
`python main.py --low C4 --high C5 --repeat 8 C4M pr4`
